https://pslab.io/

# OpenTAP PSLab Plugin

A free, open-source plugin for the OpenTAP testing automation framework to interact with a PSLab board.

## Installing/Building

TODO: Include this plugin in the OpenTAP Package Manager.

### Dependencies

This package depends on the Python OpenTAP package (v3.0.0) and the `pslab` Python package (v2.5.0).

### Building

1. Clone the contents of this repository into %TAP_PATH%/Packages/Python/PSLab.
2. Navigate to %TAP_PATH% and run the command `tap python build PSLab`.
3. The package should be built and ready to go!
